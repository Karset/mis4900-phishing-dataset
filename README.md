This repository consists of the phishing dataset generated through the work depicted in the thesis "Analysing Email Phishing Trends Through the Creation of an Email Phishing Collection Model".

The dataset is available both in an excel file with the filtering capability applied, and in a pure txt file (comma delimited).

The dataset is a part of the Master Thesis conducted in the "MIS4900 Masteroppgave informasjonssikkerhet" course.